
export const state = () => ({
    feed: [],
    writers: []
})

export const getters = {
    feed: state => state.feed,
    writers: state => state.writers
}

export const mutations = {
    POST_MESSAGE (state, {message}) {
        state.feed.push(message);
        localStorage.setItem('iadvise_feed', JSON.stringify(state.feed));
    },
    ADD_WRITER (state, {name}) {
        state.writers.push(name);
    },
    REMOVE_WRITER (state, {name}){
        let index = state.writers.indexOf(name);
        if (index > -1) {
            state.writers.splice(index, 1);
        }
    },
    INIT_FEED (state, {data}){
        state.feed = data;
    },
    CLEAR_FEED (state, {}){
        state.feed = [];
    }
}

export const actions = {
    postMessage ({commit, state, getters}, message) {
        commit('POST_MESSAGE', {message});
    },
    addWriter ({commit, state, getters}, name) {
        commit('ADD_WRITER', {name});
    },
    removeWriter ({commit, state, getters}, name) {
        commit('REMOVE_WRITER', {name});
    },
    initFeed ({commit, state, getters}, data) {
        commit('INIT_FEED', {data});
    },
    clearFeed ({commit, state, getters}) {
        commit('CLEAR_FEED', {});
    }
}